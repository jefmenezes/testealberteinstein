﻿using Entities;
using PacienteRepo.Interfaces;
using PacienteService.Interfaces;
using System.Collections.Generic;

namespace PacienteService.Services
{
    public class PacienteService : IPacienteService
    {
        private readonly IPacienteRepository _pacienteRepository;

        public PacienteService(IPacienteRepository pacienteRepository)
        {
            _pacienteRepository = pacienteRepository;
        }

        public IEnumerable<Consulta> GetConsultasByPaciente(long idPaciente)
        {
            return _pacienteRepository.GetConsultarByPaciente(idPaciente);
        }
    }
}

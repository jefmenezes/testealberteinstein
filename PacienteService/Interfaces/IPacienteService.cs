﻿using Entities;
using System.Collections.Generic;

namespace PacienteService.Interfaces
{
    public interface IPacienteService
    {
        IEnumerable<Consulta> GetConsultasByPaciente(long idPaciente);
    }
}

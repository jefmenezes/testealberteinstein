﻿using Entities;
using MedicoService.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace NetCoreTest_AlbertEinstein.Controllers
{
    [ApiController]
    public class MedicoController : ControllerBase
    {
        private readonly IMedicoService _medicoService;

        public MedicoController(IMedicoService medicoService)
        {
            _medicoService = medicoService;
        }

        [HttpGet]
        [Route("[controller]/getAppointments/{idMedico:long}")]
        public IEnumerable<Consulta> GetConsultas(long idMedico)
        {
            return _medicoService.GetConsultarByMedico(idMedico);
        }

        [HttpPost]
        [Route("[controller]/saveAppointment")]
        public ObjectResult SaveConsulta(Consulta consulta)
        {
            //Não é o ideal tratar a lógica neste ponto
            //Se houvesse front-end nesta aplicação eu faria essa chamada antes para verificar se a data está disponível
            //Após o retorno da chamada informando que a data está livre eu chamaria o método de salvar consulta
            if (!_medicoService.IsDateTimeAppointmentAvailable(consulta))
            {
                return BadRequest(new { Mensagem = "Data solicitada para o agendamento indisponível." });
            }
            
            var consultaAgendada = _medicoService.SaveConsulta(consulta);

            return Ok(new { Id = consultaAgendada.Id, Mensagem = "Consulta agendada com sucesso." });
        }
    }
}

using MedicoRepo;
using MedicoRepo.Interfaces;
using MedicoRepo.Repositories;
using MedicoService.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PacienteRepo;
using PacienteRepo.Interfaces;
using PacienteRepo.Repositories;
using PacienteService.Interfaces;

namespace NetCoreTest_AlbertEinstein
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            var connection = Configuration["ConexaoMySql:MySqlConnectionString"];
            services.AddDbContext<MedicoDbContext>(options =>
                options.UseMySql(connection)
            );
            services.AddDbContext<PacienteDbContext>(options =>
                options.UseMySql(connection)
            );

            services.AddScoped<IMedicoRepository, MedicoRepository>();
            services.AddScoped<IPacienteRepository, PacienteRepository>();

            services.AddScoped<IMedicoService, MedicoService.Services.MedicoService>();
            services.AddScoped<IPacienteService, PacienteService.Services.PacienteService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

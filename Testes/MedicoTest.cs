using Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Xunit;

namespace Testes
{
    public class MedicoTest
    {
        private HttpClient _client { get; }

        public MedicoTest()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("http://localhost:5000/");
        }

        [Fact]
        public void TestSaveConsultaMedico()
        {
            var consulta = new Consulta
            {
                IdMedico = 1,
                IdPaciente = 1,
                DataConsulta = new DateTime(2020, 8, 29, 23, 59, 59)
            };

            var serializedProduto = JsonConvert.SerializeObject(consulta);
            var content = new StringContent(serializedProduto, Encoding.UTF8, "application/json");
            var response = _client.PostAsync("/medico/saveAppointment", content).Result;

            Assert.True(response.IsSuccessStatusCode);
        }

        [Fact]
        public void TestSaveConsultaMedicoSemHorarioVago()
        {
            var consulta = new Consulta
            {
                IdMedico = 1,
                IdPaciente = 1,
                DataConsulta = new DateTime(2020, 8, 29, 23, 59, 59)
            };

            var serializedProduto = JsonConvert.SerializeObject(consulta);
            var content = new StringContent(serializedProduto, Encoding.UTF8, "application/json");
            var response = _client.PostAsync("/medico/saveAppointment", content).Result;

            Assert.True(response.StatusCode == System.Net.HttpStatusCode.BadRequest);
        }

        [Fact]
        public void TestGetConsultaMedico()
        {
            HttpResponseMessage response = _client.GetAsync("/medico/getAppointments/1").Result;
            if (response.IsSuccessStatusCode)
            {
                var consultasString = response.Content.ReadAsStringAsync().Result;
                var consultas = JsonConvert.DeserializeObject<List<Consulta>>(consultasString);

                Assert.True(consultas.Any());
            }
        }
    }
}

﻿using Entities;
using System.Collections.Generic;

namespace MedicoRepo.Interfaces
{
    public interface IMedicoRepository
    {
        IEnumerable<Consulta> GetConsultasByMedico(long idMedico);
        Consulta SaveConsulta(Consulta consulta);
        bool IsDateTimeAppointmentAvailable(Consulta consulta);
    }
}

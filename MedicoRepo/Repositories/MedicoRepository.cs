﻿using Entities;
using MedicoRepo.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace MedicoRepo.Repositories
{
    public class MedicoRepository : IMedicoRepository
    {
        private readonly MedicoDbContext _medicoContext;

        public MedicoRepository(MedicoDbContext medicoContext)
        {
            _medicoContext = medicoContext;
        }

        public IEnumerable<Consulta> GetConsultasByMedico(long idMedico)
        {
            return _medicoContext.Consultas.Where(s => s.IdMedico == idMedico);
        }

        public Consulta SaveConsulta(Consulta consulta)
        {
            _medicoContext.Consultas.Add(consulta);

            _medicoContext.SaveChanges();

            return consulta;
        }

        public bool IsDateTimeAppointmentAvailable(Consulta consulta)
        {
            return !_medicoContext.Consultas.Any(s => s.DataConsulta == consulta.DataConsulta);
        }
    }
}

﻿using Entities;
using Microsoft.EntityFrameworkCore;

namespace MedicoRepo
{
    public class MedicoDbContext : DbContext
    {
        public MedicoDbContext(DbContextOptions<MedicoDbContext> options) : base(options)
        {
        }

        public DbSet<Medico> Medicos { get; set; }

        public DbSet<Consulta> Consultas { get; set; }
    }
}

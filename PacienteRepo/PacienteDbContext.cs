﻿using Entities;
using Microsoft.EntityFrameworkCore;

namespace PacienteRepo
{
    public class PacienteDbContext : DbContext
    {
        public PacienteDbContext(DbContextOptions<PacienteDbContext> options) : base(options)
        {
        }

        public DbSet<Consulta> Consultas { get; set; }
    }
}

﻿using Entities;
using PacienteRepo.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace PacienteRepo.Repositories
{
    public class PacienteRepository : IPacienteRepository
    {
        private readonly PacienteDbContext _pacienteContext;

        public PacienteRepository(PacienteDbContext pacienteContext)
        {
            _pacienteContext = pacienteContext;
        }

        public IEnumerable<Consulta> GetConsultarByPaciente(long idPaciente)
        {
            return _pacienteContext.Consultas.Where(s => s.IdPaciente == idPaciente);
        }
    }
}

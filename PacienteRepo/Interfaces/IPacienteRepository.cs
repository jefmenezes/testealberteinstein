﻿using Entities;
using System.Collections.Generic;

namespace PacienteRepo.Interfaces
{
    public interface IPacienteRepository
    {
        IEnumerable<Consulta> GetConsultarByPaciente(long idPaciente);
    }
}

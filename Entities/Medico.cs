﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("medico")]
    public class Medico
    {
        public long Id { get; set; }
        public string Nome { get; set; }
    }
}

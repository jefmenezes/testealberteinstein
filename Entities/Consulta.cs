﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("consulta")]
    public class Consulta
    {
        public long Id { get; set; }
        public long IdMedico { get; set; }
        public long IdPaciente { get; set; }
        public DateTime DataConsulta { get; set; }
    }
}

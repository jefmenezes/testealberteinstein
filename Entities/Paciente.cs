﻿using System;

namespace Entities
{
    public class Paciente
    {
        public long Id { get; set; }
        public string Nome { get; set; }
    }
}

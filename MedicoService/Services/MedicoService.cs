﻿using Entities;
using MedicoRepo.Interfaces;
using MedicoService.Interfaces;
using System.Collections.Generic;

namespace MedicoService.Services
{
    public class MedicoService : IMedicoService
    {
        private readonly IMedicoRepository _medicoRepository;

        public MedicoService(IMedicoRepository medicoRepository)
        {
            _medicoRepository = medicoRepository;
        }

        public IEnumerable<Consulta> GetConsultarByMedico(long idMedico)
        {
            return _medicoRepository.GetConsultasByMedico(idMedico);
        }

        public bool IsDateTimeAppointmentAvailable(Consulta consulta)
        {
            return _medicoRepository.IsDateTimeAppointmentAvailable(consulta);
        }

        public Consulta SaveConsulta(Consulta consulta)
        {
            return _medicoRepository.SaveConsulta(consulta);
        }
    }
}

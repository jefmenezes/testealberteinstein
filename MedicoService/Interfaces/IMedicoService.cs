﻿using Entities;
using System.Collections.Generic;

namespace MedicoService.Interfaces
{
    public interface IMedicoService
    {
        IEnumerable<Consulta> GetConsultarByMedico(long idMedico);
        Consulta SaveConsulta(Consulta consulta);
        bool IsDateTimeAppointmentAvailable(Consulta consulta);
    }
}
